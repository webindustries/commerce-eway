<?php

namespace Drupal\eway_gate;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;

interface TransactionManagerInterface {

  /**
   * Set API Access Key.
   *
   * @param String $api_key
   *
   * @return mixed
   */
  public function setApiKey(String $api_key);

  /**
   * Set API Access Password.
   *
   * @param String $api_pass
   *
   * @return mixed
   */
  public function setApiPass(String $api_pass);

  /**
   * Set API Environment type.
   * "Sandbox" or "Production" types.
   *
   * @param String $api_env
   *
   * @return mixed
   */
  public function setApiEnv(String $api_env);

  /**
   * Pre-authorisation solution allows merchants to reserve funds on a
   * customer's credit card without charging it.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method
   * @param array $payment_details
   *
   * @return mixed
   */
  public function preAuthorizeTransaction(PaymentMethodInterface $payment_method, array $payment_details);

  /**
   * Allows for purchases to be submitted directly to the API from the server.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *
   * @return mixed
   */
  public function paymentTransaction(PaymentInterface $payment);

  /**
   * Direct call to update created pre-authorized transaction.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method
   *
   * @return mixed
   */
  public function updateAuthorizedTransaction(PaymentMethodInterface $payment_method);

}
