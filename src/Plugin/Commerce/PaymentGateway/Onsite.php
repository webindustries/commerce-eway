<?php

namespace Drupal\eway_gate\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_price\Price;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\eway_gate\TransactionManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the On-site payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "eway_gate_onsite",
 *   label = "eWay (On-site)",
 *   display_label = "eWay",
 *   forms = {
 *     "add-payment-method" = "Drupal\eway_gate\PluginForm\Onsite\PaymentMethodAddForm",
 *     "edit-payment-method" = "Drupal\commerce_payment\PluginForm\PaymentMethodEditForm",
 *   },
 *   payment_method_types = {"eway_credit_card"},
 *   credit_card_types = {
 *     "amex", "dinersclub", "discover", "jcb", "maestro", "mastercard", "visa",
 *   },
 *   requires_billing_information = FALSE,
 * )
 */
class Onsite extends OnsitePaymentGatewayBase implements OnsiteInterface {

  /**
   * @var TransactionManagerInterface
   */
  protected $transaction_manager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time, TransactionManagerInterface $transaction_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);

    // You can create an instance of the SDK here and assign it to $this->api.
    // Or inject Guzzle when there's no suitable SDK.

    $this->transaction_manager = $transaction_manager;

    if (!empty($configuration['api_key'])) {
      $this->transaction_manager->setApiKey($configuration['api_key']);
    }
    if (!empty($configuration['api_pass'])) {
      $this->transaction_manager->setApiPass($configuration['api_pass']);
    }
    if (!empty($configuration['api_pnb_key'])) {
      $this->transaction_manager->setPNBApiKey($configuration['api_pnb_key']);
    }
    if (!empty($configuration['api_env'])) {
      $this->transaction_manager->setApiEnv($configuration['api_env']);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function __wakeup() {
    parent::__wakeup();

    if (!empty($this->configuration['api_key'])) {
      $this->transaction_manager->setApiKey($this->configuration['api_key']);
    }
    if (!empty($this->configuration['api_pass'])) {
      $this->transaction_manager->setApiPass($this->configuration['api_pass']);
    }
    if (!empty($this->configuration['api_pnb_key'])) {
      $this->transaction_manager->setPNBApiKey($this->configuration['api_pnb_key']);
    }
    if (!empty($this->configuration['api_env'])) {
      $this->transaction_manager->setApiEnv($this->configuration['api_env']);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('eway_gate.transaction_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'api_key' => '',
      'api_pass' => '',
      'api_pnb_key' => '',
      'api_env' => 'sandbox',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    // Credential. Also needs matching schema in
    // config/schema/$your_module.schema.yml.
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#default_value' => $this->configuration['api_key'],
      '#description' => $this->t(''),
      '#required' => TRUE
    ];

    $form['api_pass'] = [
      '#type' => 'password',
      '#title' => $this->t('API Password'),
      '#description' => $this->t(''),
      '#required' => TRUE
    ];

    $form['api_pnb_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Pay Now Button Public API Key'),
      '#default_value' => $this->configuration['api_pnb_key'],
      '#description' => $this->t('Requests to the Encryption API need to be authenticated using basic authentication. This uses the "Pay Now Button Public API Key"'),
      '#required' => TRUE
    ];

    $form['api_env'] = [
      '#type' => 'radios',
      '#title' => $this->t('API Environment'),
      '#options' => [
        'production' => $this->t('Production'),
        'sandbox' => $this->t('Sandbox')
      ],
      '#default_value' => $this->configuration['api_env'],
      '#required' => TRUE
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);

      $this->configuration['api_key'] = $values['api_key'];
      $this->configuration['api_env'] = $values['api_env'];
      $this->configuration['api_pnb_key'] = $values['api_pnb_key'];
      $this->configuration['api_pass'] = $values['api_pass'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);

    // The remote ID returned by the request.
    $remote_id = $this->transaction_manager->paymentTransaction($payment);

    if (!$remote_id) {
      throw new HardDeclineException('The payment failed.');
    }

    $next_state = $capture ? 'completed' : 'authorization';

    $payment->setRemoteId($remote_id);
    $payment->setState($next_state);
    $payment->setAvsResponseCode('A');
    if (!$payment_method->card_type->isEmpty()) {
      $avs_response_code_label = $this->buildAvsResponseCodeLabel('A', $payment_method->card_type->value);
      $payment->setAvsResponseCodeLabel($avs_response_code_label);
    }
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['authorization']);

    // If not specified, capture the entire amount.
    $amount = $amount ?: $payment->getAmount();

    $payment->setState('completed');
    $payment->setAmount($amount);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    $this->assertPaymentState($payment, ['authorization']);
    // Perform the void request here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    $remote_id = $payment->getRemoteId();

    $payment->setState('authorization_voided');
    $payment->save();
  }

  /**
   * {@inheritdoc}
   *
   * TODO: implement refund functionality.
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    $this->assertRefundAmount($payment, $amount);

    // Perform the refund request here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    $remote_id = $payment->getRemoteId();
    $number = $amount->getNumber();

    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    if ($new_refunded_amount->lessThan($payment->getAmount())) {
      $payment->setState('partially_refunded');
    }
    else {
      $payment->setState('refunded');
    }

    $payment->setRefundedAmount($new_refunded_amount);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    $required_keys = [
      // The expected keys are payment gateway specific and usually match
      // the PaymentMethodAddForm form elements. They are expected to be valid.
      'type', 'number', 'expiration', 'name'
    ];
    foreach ($required_keys as $required_key) {
      if (empty($payment_details[$required_key])) {
        throw new \InvalidArgumentException(sprintf('$payment_details must contain the %s key.', $required_key));
      }
    }

    // The remote ID returned by the request.
    $remote_id = $this->transaction_manager->preAuthorizeTransaction($payment_method, $payment_details);

    // Perform the create request here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    // You might need to do different API requests based on whether the
    // payment method is reusable: $payment_method->isReusable().
    // Non-reusable payment methods usually have an expiration timestamp.
    $payment_method->card_type = $payment_details['type'];
    // Only the last 4 numbers are safe to store.
    $payment_method->card_number = substr($payment_details['number'], -4);
    $payment_method->card_exp_month = $payment_details['expiration']['month'];
    $payment_method->card_exp_year = $payment_details['expiration']['year'];
    $payment_method->card_name = $payment_details['name'];
    $expires = CreditCard::calculateExpirationTimestamp($payment_details['expiration']['month'], $payment_details['expiration']['year']);

    $payment_method->setRemoteId($remote_id);
    $payment_method->setExpiresTime($expires);
    $payment_method->save();
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    // Delete the remote record here, throw an exception if it fails.
    // See \Drupal\commerce_payment\Exception for the available exceptions.
    // Delete the local entity.
    $payment_method->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function updatePaymentMethod(PaymentMethodInterface $payment_method) {
    $remote_id = $this->transaction_manager->updateAuthorizedTransaction($payment_method);

    if (!$remote_id) {
      throw new HardDeclineException('The payment method update failed.');
    }

    $payment_method->setRemoteId($remote_id);
    $payment_method->save();
  }

  /**
   * {@inheritdoc}
   */
  public function buildAvsResponseCodeLabel($avs_response_code, $card_type) {
    if ($card_type == 'dinersclub' || $card_type == 'jcb') {
      if ($avs_response_code == 'A') {
        return $this->t('Approved.');
      }
      return NULL;
    }
    return parent::buildAvsResponseCodeLabel($avs_response_code, $card_type);
  }

}
